using System;
using System.Collections;
using System.Collections.Generic;

namespace dotnetcore
{
    public class Produto
    {
        public string Nome;
        public int Idprod;
        public int Quantidade;
        public double Valor;
        
        public Produto()
        {
            Nome = "";
            Valor = 0;
            Quantidade = 0;
            Idprod++;
        }

        public Produto ConstrutorProduto(string nome, double valor, int quantidade)
        {
            Produto novoProduto = new Produto();
            novoProduto.Nome = nome;
            novoProduto.Valor = valor;
            novoProduto.Quantidade = quantidade;
            GuardaListas.todosProdutos.Add(novoProduto);
            return novoProduto;
        }
        
    }
}