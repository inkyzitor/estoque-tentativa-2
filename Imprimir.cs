using System;
using System.Collections.Generic;
using System.Linq;

namespace dotnetcore
{
    public class Imprimir
    {

        // public Imprimir FazerImpressao()
        // {
        //     string resposta = "";
        //     do
        //     {
        //         Console.WriteLine("--------------$-------------");
        //         Console.WriteLine(ImprimirTudo());
        //         Console.WriteLine("imprimir novo produto? (S/N)");
        //         Console.WriteLine("--------------$-------------");
        //         resposta = Convert.ToString(Console.ReadLine());
        //     } while (resposta == "S" || resposta == "s");
        //     return null;
        // }
        
        public string ImprimirTudo(List<Produto> todosProdutos)
        {
            string imprimir = "\nID\t\tNOME\t\tVALOR\t\tQUANTIDADE\n";
            foreach (Produto p in todosProdutos)
            {
                imprimir = imprimir + p.Idprod + "\t\t" + p.Nome + "\t\t" + p.Valor + "\t\t" + p.Quantidade + "\n";
            }
            return imprimir;
        }

        
    }
}