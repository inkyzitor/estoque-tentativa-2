using System.Collections.Generic;

namespace dotnetcore
{
    public class GuardaListas
    {
        public static List<Produto> todosProdutos = new();
        
        public Produto BuscarProduto(int id)
        {
            return todosProdutos.Find(produto => produto.Idprod == id);
        }

    }
    
    
}